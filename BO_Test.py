# Optimization of Rosenbrock Equation

import numpy as np
from ax import optimize

def rosenbrock(x, a=1, b=100):
    return (a - x[0]) ** 2 + b * (x[1] - x[0] ** 2) ** 2
def rosenbrock_evaluation_function(parameterization):
    x = np.array([parameterization.get(f"x{i + 1}") for i in range(2)])
    return rosenbrock(x)


best_parameters, values, experiment, model = optimize(
    parameters=[
        {
            "name": "x1",
            "type": "range",
            "bounds": [-10000, 10000],
            "value_type": "float",
        },
        {
            "name": "x2",
            "type": "range",
            "bounds": [-10000, 10000],
            "value_type": "float",
        },
    ],
    evaluation_function=rosenbrock_evaluation_function,
    minimize=True,
    total_trials=15,
        )

print(best_parameters)

means, covariances = values
print(means)

